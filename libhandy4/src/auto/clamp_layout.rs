// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use glib::object::Cast;
use glib::object::ObjectType as ObjectType_;
use glib::signal::connect_raw;
use glib::signal::SignalHandlerId;
use glib::translate::*;
use glib::StaticType;
use glib::ToValue;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;

glib::wrapper! {
    pub struct ClampLayout(Object<ffi::HdyClampLayout, ffi::HdyClampLayoutClass>) @extends gtk::LayoutManager, @implements gtk::Orientable;

    match fn {
        get_type => || ffi::hdy_clamp_layout_get_type(),
    }
}

impl ClampLayout {
    #[doc(alias = "hdy_clamp_layout_new")]
    pub fn new() -> ClampLayout {
        assert_initialized_main_thread!();
        unsafe { gtk::LayoutManager::from_glib_full(ffi::hdy_clamp_layout_new()).unsafe_cast() }
    }

    #[doc(alias = "hdy_clamp_layout_get_maximum_size")]
    pub fn get_maximum_size(&self) -> i32 {
        unsafe { ffi::hdy_clamp_layout_get_maximum_size(self.to_glib_none().0) }
    }

    #[doc(alias = "hdy_clamp_layout_get_tightening_threshold")]
    pub fn get_tightening_threshold(&self) -> i32 {
        unsafe { ffi::hdy_clamp_layout_get_tightening_threshold(self.to_glib_none().0) }
    }

    #[doc(alias = "hdy_clamp_layout_set_maximum_size")]
    pub fn set_maximum_size(&self, maximum_size: i32) {
        unsafe {
            ffi::hdy_clamp_layout_set_maximum_size(self.to_glib_none().0, maximum_size);
        }
    }

    #[doc(alias = "hdy_clamp_layout_set_tightening_threshold")]
    pub fn set_tightening_threshold(&self, tightening_threshold: i32) {
        unsafe {
            ffi::hdy_clamp_layout_set_tightening_threshold(
                self.to_glib_none().0,
                tightening_threshold,
            );
        }
    }

    pub fn connect_property_maximum_size_notify<F: Fn(&ClampLayout) + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn notify_maximum_size_trampoline<F: Fn(&ClampLayout) + 'static>(
            this: *mut ffi::HdyClampLayout,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::maximum-size\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_maximum_size_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    pub fn connect_property_tightening_threshold_notify<F: Fn(&ClampLayout) + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn notify_tightening_threshold_trampoline<
            F: Fn(&ClampLayout) + 'static,
        >(
            this: *mut ffi::HdyClampLayout,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::tightening-threshold\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_tightening_threshold_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl Default for ClampLayout {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Clone, Default)]
pub struct ClampLayoutBuilder {
    maximum_size: Option<i32>,
    tightening_threshold: Option<i32>,
    orientation: Option<gtk::Orientation>,
}

impl ClampLayoutBuilder {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn build(self) -> ClampLayout {
        let mut properties: Vec<(&str, &dyn ToValue)> = vec![];
        if let Some(ref maximum_size) = self.maximum_size {
            properties.push(("maximum-size", maximum_size));
        }
        if let Some(ref tightening_threshold) = self.tightening_threshold {
            properties.push(("tightening-threshold", tightening_threshold));
        }
        if let Some(ref orientation) = self.orientation {
            properties.push(("orientation", orientation));
        }
        let ret = glib::Object::new::<ClampLayout>(&properties).expect("object new");
        ret
    }

    pub fn maximum_size(mut self, maximum_size: i32) -> Self {
        self.maximum_size = Some(maximum_size);
        self
    }

    pub fn tightening_threshold(mut self, tightening_threshold: i32) -> Self {
        self.tightening_threshold = Some(tightening_threshold);
        self
    }

    pub fn orientation(mut self, orientation: gtk::Orientation) -> Self {
        self.orientation = Some(orientation);
        self
    }
}

impl fmt::Display for ClampLayout {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("ClampLayout")
    }
}
